#!/bin/sh
set -e

cd samples
rm -rf Makefile
ln -sf ../debian/Makefile.samples Makefile
make test
make clean
rm -rf Makefile
